<?php
/**
 * FeedAggregator Node
 * @description: Generates 1st class nodes from aggregator feed items.
 *
 * @author: Mike Carter <mike @ www.ixis.co.uk/contact>
 */

function feedaggregator_node_help($section){
  switch($section){
    case 'node/add#aggregator-item':
      return t('A news item.');
    case "admin/modules#description":
      return t('<strong>Feed Processor</strong>: Creates nodes from feed items');
  }
}


function feedaggregator_node_node_info() {
  return array('aggregator-item' => array('name' => t('aggregator item'), 'base' => 'feedaggregator_node'));
}

 
function feedaggregator_node_perm() {
  return array('edit aggregator items');
}


function feedaggregator_node_access($op, $node) {
  global $user;

  if ($op == 'create') {
    // Only users with permission to do so may create this node type.
    return user_access('administer news feeds');
  }

  // Users who create a node may edit or delete it later, assuming they have the
  // necessary permissions.
  if ($op == 'update' || $op == 'delete') {
    if (user_access('edit aggregator items') && ($user->uid == $node->uid)) {
      return TRUE;
    }
  }
}


/**
 * Implementation of hook_menu().
 */
function feedaggregator_node_menu($may_cache) {
  global $user;
  $items = array();

  if ($may_cache) {
    $items[] = array('path' => 'node/add/aggregator-item', 'title' => t('aggregator-item'),
      'access' => user_access('edit aggregator items'));
  }

  return $items;
}


function feedaggregator_node_form(&$node) {
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#required' => TRUE,
    '#default_value' => $node->title,
    '#weight' => -5,
    '#description' => t('A short descriptive title for the story.')
  );

  $form['body_filter']['body'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => $node->body,
    '#rows' => '10',
    '#required' => FALSE
  );
  $form['body_filter']['filter'] = filter_form($node->format);

  $form['link'] = array(
    '#type' => 'textfield',
    '#title' => t('Link'),
    '#required' => TRUE,
    '#default_value' => $node->link,
    '#weight' => -4,
    '#description' => t('The fully-qualified URL to the complete story.')
  );

  return $form;
}


function feedaggregator_node_link($type, $node = 0, $teaser = FALSE) {
  $links = array();

  if ($type == 'node' && $node->type == 'aggregator-item') {
    if (!$teaser && variable_get('feedmanager_showfullstory', TRUE)) {
      // Make the full story url in to a clickable link when viewing the node
      $attributes['target'] = variable_get('feedmanager_target', 'default');
      if($rel = variable_get('feedmanager_rel', false)) $attributes['rel'] = $rel;

      $links[] = l(t('full story'), $node->link, $attributes);
    }
  }

  return $links;
}


function feedaggregator_node_update($node) {
  db_query("UPDATE {aggregator_node} SET link = '%s' WHERE nid = %d", $node->link, $node->nid);
}


function feedaggregator_node_load($node) {
  $result = db_query('SELECT af.title as feed_name, af.url as feed_url, af.image as feed_image, an.fid, an.iid, an.link FROM {aggregator_node} an LEFT JOIN {aggregator_feed} af ON af.fid = an.fid WHERE nid = %d', $node->nid);
  $additions = db_fetch_object($result);
  
  return $additions;
}


function feedaggregator_node_delete(&$node) {
  db_query('DELETE FROM {aggregator_node} WHERE nid = %d', $node->nid);
}


function feedaggregator_node_insert($node) {
  db_query("INSERT INTO {aggregator_node} SET iid = %d, nid = %d, fid = %d, link = '%s'", $node->iid, $node->nid, $node->fid, $node->link);
}


function feedaggregator_node_parsername() {
	return array('aggregator-item');
}

function feedaggregator_get_vocabulary() {
	return db_result(db_query("SELECT v.vid FROM {vocabulary} v INNER JOIN {vocabulary_node_types} n ON v.vid = n.vid WHERE n.type = '%s' ORDER BY v.weight, v.name", 'aggregator-item'));
}

function feedaggregator_node_feedapi($feed, $op, $item = NULL) {
  global $user;
  static $cat_names = array();

  switch($op) {
  	case 'processor_name':
  		return array('feedaggregator_node' => t('Nodes'));

    case 'item_count':
      $total_items = db_result(db_query('SELECT COUNT(an.nid) FROM {node} n JOIN {aggregator_node} an ON n.nid = an.nid WHERE an.fid = %d AND n.status = 1', $feed['fid']));
      return array('item_count' => $total_items);

    case 'item_save':
      if($feed['processor'] == 'feedaggregator_node') {

        $node['link'] = $item->get_link();

				// Check if the item already exists
				$duplicate = db_fetch_array(db_query("SELECT nid FROM {aggregator_node} WHERE link = '%s'", $node['link']));
				if($duplicate) {
  				$old_node = (array)node_load(array('nid' => $duplicate['nid']));

  				// Should this feed honor updating of existing items?
  				if(!$feed['update_items'] || (
  				    strcasecmp($old_node['body'], $item->get_description()) == 0
  				     && strcasecmp($old_node['title'], $item->get_title()) == 0
  				     && strcasecmp($old_node['link'], $item->get_link()) == 0
  				    )) {
    				return FALSE;
  				}

					$node['nid'] = $old_node['nid'];
					$node['created'] = $old_node['created'];
				} else {
					// Set nodes default options
					$options = variable_get('node_options_aggregator-item', array('status', 'promote'));
					foreach($options as $option) {
						$node[$option] = 1;
					}

					$item_time = $item->get_date('U');
					$node['created'] = $item_time ? $item_time : time();
          $node->date = format_date($node['created'], 'custom', 'Y-m-d H:i:s O'); 
				}
        
        $title = $item->get_title();
        
        $node += array(
					'type' => 'aggregator-item',
					'changed' => time(),
					'comment' => variable_get('comment_aggregator-item', 2),
					'title' => parse_entities($title),
					'teaser' => $item->get_description() ? node_teaser($item->get_description(), $feed['format']) : '',
					'body' => $item->get_description(),
					'fid' => $feed['fid'],
					'iid' => $item->data['iid'],
					'taxonomy' => $feed['taxonomy'],
					'format' => $feed['format']
				);
        
				// Assign the author as specified in this feeds settings
        $account = user_load(array('name' => $feed['author']));
        $node['uid'] = $account->uid;

				// Convert Node array in to an object
				$node = (object)$node;

				if($feed['autotaxonomy'] && is_array($item->get_categories())) {
					// Find the vocabulary associated with this node type
					$vocab = feedaggregator_get_vocabulary();

					// Process any feed item category tags
					if($vocab) {
						foreach ($item->get_categories() as $category_name) {
							if (trim($category_name) != '') {

								// See if the term is already defined
								if (!isset($cat_names[$category_name])) {
									$cat_names[$category_name] = module_invoke('taxonomy', 'get_term_by_name', $category_name);
								}

								// If there is no existing term, create a new one
								if (count($cat_names[$category_name]) == 0) {
									$term = array();
									$term['name'] = check_plain($category_name);
									$term['description'] = '';
									$term['vid'] = $vocab;
									$term['weight'] = 0;
									module_invoke('taxonomy', 'save_term', $term);

									// Assign the new term to our new node
									$node->taxonomy[] = $term['tid'];
									$cat_names[$category_name][0]->tid = $term['tid'];
								}
								else {
									// Use the existing category term in the database
									if (!in_array($cat_names[$category_name][0]->tid, $node->taxonomy)) {
										$node->taxonomy[] = $cat_names[$category_name][0]->tid;
									}
								}
							}
						}
					}
				}

        node_object_prepare($node);
        $node = node_submit($node);
				node_save($node);
				return array('item_save' => TRUE);
      }
      break;

    case 'expire_items':
    	// Unpublish any old nodes
     	$result = db_query(db_rewrite_sql("UPDATE {node} n JOIN {aggregator_node} an ON n.nid = an.nid SET n.status = 0 WHERE an.fid = %d AND n.changed < %d"), $feed['fid'], time() - $feed['expires']);
      break;

    case 'remove':
      $result = db_query("SELECT nid FROM {aggregator_node} WHERE fid = %d", $feed['fid']);
      while($node = db_fetch_object($result)) {
      	node_delete($node->nid);
      }

      break;
  }
}


function feedaggregator_node_form_alter($form_id, &$form) {
  switch($form_id) {
    case 'feedmanager_form_feed_edit':
      // Show extra node options if the processor is 'nodes'.
      if($form['advanced']['processor']['#value'] == 'feedaggregator_node') {
        $feed = feedmanager_get_feed($form['fid']['#value']);
        $form['advanced']['author'] = array('#type' => 'textfield', '#title' => t('Authored by'), '#maxlength' => 60, '#autocomplete_path' => 'user/autocomplete', '#default_value' => $feed['author'] ? $feed['author'] : '', '#weight' => -1, '#description' => t('Assigns created nodes to a user. Leave blank for %anonymous.', array('%anonymous' => theme('placeholder', variable_get('anonymous', 'Anonymous')))));

				// AutoTaxonomy
				if(feedaggregator_get_vocabulary()) {
					$form['advanced']['autotaxonomy'] = array(
						'#type' => 'checkbox',
						'#title' => t('Use feed category tags'),
						'#description' => t('Some feeds provide categories defined by the author. These can be added to your taxonomy, and the feed items tagged with the terms.'),
						'#default_value' => $edit['autotaxonomy'] ? $edit['autotaxonomy'] : FALSE,
					);
				} else {
					$form['advanced']['autotaxonomy'] = array(
						'#value' => t('You can automatically use tags defined in this feed to categories your nodes as they are created. To do this you need to %url to the %type content-type.', array('%url' => l(t('create a vocabulary and assign it'), 'admin/taxonomy'), '%type' => theme('placeholder', 'aggregator-item')))
					);
				}
				
        // Define what Filters to pass aggregator items through
        $form['advanced']['format'] = filter_form($feed['format'], 0, array('advanced', 'format'));


        $form['advanced']['format']['#collapsed'] = FALSE;
        $form['advanced']['format']['#collapsible'] = FALSE;
      }
      break;
  }
}


/*
 * Lists all items in the feed
 */
function feedaggregator_node_list_items($feed, $limit = NULL) {
  $per_page = $limit ? $limit : variable_get('default_nodes_main', 10);
  $result = pager_query(db_rewrite_sql('SELECT n.nid, n.sticky, n.created FROM {node} n JOIN {aggregator_node} an ON an.nid = n.nid WHERE an.fid = %d AND n.status = 1 ORDER BY n.sticky DESC, n.created DESC'), $per_page, $feed['fid'], NULL, $feed['fid']);

  if (db_num_rows($result)) {
    drupal_add_link(array('rel' => 'alternate',
                          'type' => 'application/rss+xml',
                          'title' => t('RSS'),
                          'href' => url('rss.xml', NULL, NULL, TRUE)));
    $output = '';
    while ($node = db_fetch_object($result)) {
      $output .= node_view(node_load($node->nid), 1);
    }

    $output .= theme('pager', NULL, $per_page, $feed['fid']);
  }

  return $output;
}
