Package: Feed Parser
Author: Mike Carter <www.ixis.co.uk/contact>


Description

This package provides a suite of tools to manage, process, and display RSS/ATOM data feeds. It is intended as a direct replacement for the Drupal core aggregator.module - as such it will modify the existing aggregator database tables for it's own use.


Features

* Per feed configuration options for ultimate content flexibility.
* Independant feed processing engines to create whatever content you want from feed items.
* Feeds parsed by the solid and up to date SimplePie [simplepie.org] parser engine.
* Flexible FeedAPI for manipulating and processing feed data as it's parsed.


Installation

The package includes the following modules. Not all of which are required for simple usage.

feedmanager          Provides an admin interface to manage your feeds, and process them via cron.
feedaggregator_node  A feed processor which converts items in to 1st class Drupal nodes.
feedaggregator       A feed processor which converts items in to traditional Drupal aggregator items.
feedenclosure        Extracts enclosure attachment data (aka Podcasts) and stores them in the database.

To get started activate the 'feedmanager' and 'feedaggregator_node' modules.


Creating A Feed

A feed requires a working URL to a feed, along with a title. The description field is pulled out from the feed source.

Update and Discard are self explanatory.

In the 'Advanced' box you must select what feed processor should be used to process the feed items. Each feed can have its own processor.


What Is A Processor?

The processor modules consume feed items and output something based on the items content. Examples for what a processor might do include:

* automatically email each feed item to users.
* create nodes (as done by feedaggregator_node.module)
* create old aggregator.module items (to allow using other modules which work with aggregator.module data - like news_page.module)

Each processor provides custom settings which can be defined on the feed's setting page. Once a processor has been selected you will need to save the feed and then re-edit the feed to reveal the extra settings.


Categorising Content

Each feed can be assigned a number of tags via the Drupal free-tagging system. To do this you must first associate a vocabulary with the Feedparser system.

If you need one, create a vocabulary in the usual way at admin/taxonomy. You can now associate the vocabulary with the Feedparser system on the admin/settings/feedmanager page. All vocabularies are available in the 'Vocabulary' drop down, just pick the one you wish to store your tags in.

Once a vocabulary has been set-up a new form field will be available when creating a feed at admin/aggregator.  Any terms associated with a feed are automatically applied to any feed items created.


Processor: Nodes

Creates 1st class Drupal nodes from feed items. The node will be of type 'aggregator-item'.

Settings for nodes include the author, body filter mode, and feed category tags.

To use the automatic taxonomy creation you must have a vocabulary associated with the 'aggregator-item' content-type.

Some feeds provide tags per item which the author has defined. If enabling 'Use feed category tags' these tags will be extracted from feeds and added to the sites taxonomy - and associated with the node in question as it is created.



